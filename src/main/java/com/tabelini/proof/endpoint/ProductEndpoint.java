package com.tabelini.proof.endpoint;

import com.tabelini.proof.model.Image;
import com.tabelini.proof.model.Product;
import com.tabelini.proof.service.ProductService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.print.attribute.standard.Media;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.*;

/**
 * Product Rest Endpoint
 */
@Component
@Path("/products")
@Slf4j
public class ProductEndpoint {

    public static final String PRODUCT_NOT_FOUND = "product not found";
    @Autowired
    private ProductService service;

    @GET
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getProductById(@PathParam("id") long id) {
        Product product = service.findById(id, true);
        //if the product is not found returns an 404 with an message
        if (product == null) {
            log.info("Product id:{} not found!", id);
            return createErrorMessage(PRODUCT_NOT_FOUND, 404);
        }
        return Response.status(200).entity(product).build();
    }

    @GET
    @Path("/{id}/no_relation")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getProductByIdWithoutRelationships(@PathParam("id") long id) {
        Product product = service.findById(id);
        //if the product is not found returns an 404 with an message
        if (product == null) {
            log.info("Product id:{} not found!", id);
            return createErrorMessage(PRODUCT_NOT_FOUND, 404);
        }
        return Response.status(200).entity(product).build();
    }


    @POST
    @Produces(MediaType.APPLICATION_JSON)
    public Response createProduct(Product product) {
        if (product.getId() != 0) {
            log.warn("Tried to persisted already persisted entity:'{}'", product);
            return createErrorMessage("product already persisted", 400);
        }
        try {
            return Response.status(201).entity(service.save(product)).build();
        } catch (Exception e) {
            return createErrorMessage("exception: " + e.getMessage(), 500);
        }
    }

    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    public Response updateProduct(Product product) {
        if (product.getId() == 0) {
            log.warn("Tried to update a not persisted entity:'{}'", product);
            return createErrorMessage("product not persisted", 400);
        }
        try {
            if (service.findById(product.getId(),true) == null) {
                log.warn("Tried to update an entity that cannot be found: '{}'");
                return createErrorMessage(PRODUCT_NOT_FOUND, 404);
            }
            Product updatedProduct = service.save(product);
            return Response.status(202).entity(updatedProduct).build();
        } catch (Exception e) {
            return createErrorMessage("exception: " + e.getMessage(), 500);
        }
    }

    @DELETE
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response deleteProduct(@PathParam("id") long id) {
        try {
            Product deletedProduct = service.deleteById(id);
            if (deletedProduct == null) {
                log.warn("Tried to delete an entity that cannot be found: '{}'");
                return createErrorMessage(PRODUCT_NOT_FOUND, 404);
            }
            return Response.status(202).entity(deletedProduct).build();
        } catch (Exception e) {
            return createErrorMessage("exception: " + e.getMessage(), 500);
        }
    }


    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getAll(){
        try{
            return Response.status(200).entity(service.findAll(true)).build();
        }catch (Exception e){
            return createErrorMessage("exception: "+ e.getMessage(), 500);
        }
    }


    @GET
    @Path("/no_relation")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getAllWithoutRelationships(){
        try{
            return Response.status(200).entity(service.findAll()).build();
        }catch (Exception e){
            return createErrorMessage("exception: "+ e.getMessage(), 500);
        }
    }

    @GET
    @Path("/{id}/images")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getProductImagesById(@PathParam("id") long id) {
        Product product = service.findById(id, true);
        //if the product is not found returns an 404 with an message
        if (product == null) {
            log.info("Product id:{} not found!", id);
            return createErrorMessage(PRODUCT_NOT_FOUND, 404);
        }
        List<Map<String, String>> fieldList = new ArrayList<>();
        for (Image image : product.getImageList()) {
            Map<String, String> fields = new TreeMap<>();
            fields.put("id",""+image.getId());
            fields.put("type",image.getType());
            fieldList.add(fields);
        }
        return Response.status(200).entity(fieldList).build();
    }

    @GET
    @Path("/{id}/child")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getProductChildsById(@PathParam("id") long id) {
        Product product = service.findById(id, true);
        //if the product is not found returns an 404 with an message
        if (product == null) {
            log.info("Product id:{} not found!", id);
            return createErrorMessage(PRODUCT_NOT_FOUND, 404);
        }
        List<Map<String, String>> fieldList = new ArrayList<>();
        for (Product childProduct : product.getChildProductList()) {
            Map<String, String> fields = new TreeMap<>();
            fields.put("id",""+childProduct.getId());
            fields.put("name",childProduct.getName());
            fields.put("description",childProduct.getDescription());
            fieldList.add(fields);
        }
        return Response.status(200).entity(fieldList).build();
    }


    protected Response createErrorMessage(String message, int status) {
        Map<String, String> responseText = new HashMap<>();
        responseText.put("message", message);
        return Response.status(status).entity(responseText).build();
    }

}
