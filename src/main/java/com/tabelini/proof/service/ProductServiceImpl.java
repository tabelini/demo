package com.tabelini.proof.service;


import com.tabelini.proof.model.Image;
import com.tabelini.proof.model.Product;
import com.tabelini.proof.persistence.repositories.ProductRepository;
import org.hibernate.Hibernate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;
import java.util.ArrayList;
import java.util.List;

/**
 * Product Service implemented by spring data repository
 */
@Service
public class ProductServiceImpl implements ProductService {

    private static final String DEFAULT_NAME = "some name";
    private static final String DEFAULT_DESCRIPTION = "some description";

    @Autowired
    protected ProductRepository repository;


    /**
     * Initializes the repository to have something to test
     */
    @PostConstruct
    public void initRepository() {
        Product product = createDummyProduct("", 3, null);
        Product product1 = createDummyProduct("1", 3, product);
        Product product2 = createDummyProduct("2", 3, product);
        Product product3 = createDummyProduct("3", 3, product);
        List<Product> childProductList = new ArrayList<>();
        childProductList.add(product1);
        childProductList.add(product2);
        childProductList.add(product3);
        product.setChildProductList(childProductList);
        repository.save(product);
    }

    @Override
    public Product findById(long id) {
        return this.findById(id, false);
    }

    @Override
    @Transactional(readOnly = true)
    public Product findById(long id, boolean withRelationships) {
        Product product = repository.findOne(id);
        if (product != null && withRelationships) this.fetchCollections(product);
        return product;
    }

    @Override
    @Transactional
    public Product save(Product product) {
        if (product != null) this.fetchCollections(product);
        Product p = repository.save(product);
        return p;
    }

    @Override
    @Transactional
    public Product deleteById(long id) {
        Product product = this.findById(id);
        if (product != null) repository.delete(id);
        return product;
    }

    @Override
    @Transactional
    public List<Product> findAll(){
        return this.findAll(false);
    }

    @Override
    @Transactional
    public List<Product> findAll(boolean withRelationships){
        List<Product> productList = repository.findAll();
        if(withRelationships){
            for (Product product : productList) {
                if(product != null) this.fetchCollections(product);
            }
        }
        return productList;
    }

    protected void fetchCollections(Product product) {
        Hibernate.initialize(product.getImageList());
        Hibernate.initialize(product.getChildProductList());
        if (product.getChildProductList() != null) {
            for (Product p : product.getChildProductList()) {
                this.fetchCollections(p);
            }
        }
    }

    public Product createDummyProduct(String suffix, int images, Product parentProduct) {
        Product product = new Product(DEFAULT_NAME + suffix, DEFAULT_DESCRIPTION + suffix);
        product.setParentProduct(parentProduct);
        List<Image> imageList = new ArrayList<>();
        for (int i = 0; i < images; i++) {
            imageList.add(new Image("type" + i, product));
        }
        product.setImageList(imageList);
        return product;
    }
}
