package com.tabelini.proof.service;


import com.tabelini.proof.model.Product;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Product Service
 */
public interface ProductService {

    /**
     * Finds a product by the database id
     * @param id - database id
     * @return product with that id of null if not found
     */
    public Product findById(long id);

    /**
     * Finds a product by the database id and brings the relationships
     * @param id - database id
     * @param withRelationships - true retrieves the relationships from database
     * @return product with that id of null if not found
     */
    public Product findById(long id, boolean withRelationships);

    /**
     * Saves a product in the database
     * @param product - product to be saved
     * @return product with the id set
     */
    public Product save(Product product);

    /**
     * Deletes a product in the database
     * @param id - id to delete
     * @return product deleted
     */
    public Product deleteById(long id);

    /**
     * Gets all products in the database
     * @return a list with all products
     */
    public List<Product> findAll();

    /**
     * Gets all products in the database and brings the relationships
     * @param withRelationships - true retrieves the relationships from database
     * @return a list with all products
     */
    public List<Product> findAll(boolean withRelationships);
}
