package com.tabelini.proof.model;

import com.fasterxml.jackson.annotation.*;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

/**
 * Represents an Image
 */
@Data
@NoArgsConstructor
@RequiredArgsConstructor
@Entity
@Table(name = "image")
@JsonIdentityInfo(scope = Image.class, generator = ObjectIdGenerators.None.class, property="id")
public class Image {

    //TODO constructor

    /**
     * Database Id
     */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    /**
     * Type of image??
     */
    @NotNull
    @NonNull
    private String type;

    @ManyToOne
    @JoinColumn(name="product_id")
    @NonNull
    private Product product;


    //TODO:write the get, set, hash, equals and toString by hand to show that i can do it
    //TODO: test lazy loading of image blobs


    @Override
    public String toString() {
        return "Image{" +
                "id=" + id +
                ", type='" + type + '\'' +
                ", product_id=" + product.getId() +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Image image = (Image) o;

        if (id != image.id) return false;
        if (!type.equals(image.type)) return false;
        return product.getId() == image.product.getId();
    }

    @Override
    public int hashCode() {
        int result = (int) (id ^ (id >>> 32));
        result = 31 * result + type.hashCode();
        result = 31 * result + (new Long(product.getId())).hashCode();
        return result;
    }
}
