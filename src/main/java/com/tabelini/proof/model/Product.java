package com.tabelini.proof.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * Represents a Product
 */

@Data
@NoArgsConstructor
@RequiredArgsConstructor
@Entity
@Table(name = "product")
@JsonIdentityInfo(scope = Product.class, generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
public class Product {

    //TODO: constructor

    /**
     * Database Id
     */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    /**
     * Product Name
     */
    @NonNull
    @NotNull
    private String name;

    /**
     * Product description
     */
    @NonNull
    private String description;

    /**
     * Images of the product
     * Not asked in the test description but felt the need to create it
     */
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "product", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Image> imageList;


    /**
     * Parent Product
     */
    @ManyToOne
    @JoinColumn(name = "parent_product_id")
    private Product parentProduct;

    /**
     * Child products
     */
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "parentProduct", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Product> childProductList;

    //TODO:write the get, set, hash, equals and toString by hand to show that i can do it
    //TODO: test lazy loading of image blobs


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Product product = (Product) o;

        if (id != product.id) return false;
        if (!name.equals(product.name)) return false;
        if (!description.equals(product.description)) return false;
        if (!imageList.equals(product.imageList)) return false;
        if (parentProduct != null) {
            if (!(parentProduct.id == product.parentProduct.id)) return false;
        }
        if(childProductList != null){
            return childProductList.equals(product.childProductList);
        }else return true;
    }

    @Override
    public int hashCode() {
        int result = (int) (id ^ (id >>> 32));
        result = 31 * result + name.hashCode();
        result = 31 * result + description.hashCode();
        result = 31 * result + imageList.hashCode();
        if (parentProduct != null) {
            result = 31 * result + (new Long(parentProduct.id)).hashCode();
        }
        if(childProductList != null){
            result = 31 * result + childProductList.hashCode();
        }
        return result;
    }

    @Override
    public String toString() {
        return "Product{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", imageList=" + imageList +
                ((parentProduct != null) ? ", parentProductId=" + parentProduct.id : "null") +
                ", childProductList=" + childProductList +
                '}';
    }
}
