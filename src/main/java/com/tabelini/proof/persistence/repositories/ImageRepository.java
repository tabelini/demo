package com.tabelini.proof.persistence.repositories;

import com.tabelini.proof.model.Image;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Spring Data Product repository
 */
public interface ImageRepository extends JpaRepository<Image,Long> {
    //TODO:make a JPA repository
    //TODO:make a JDBC repository
}
