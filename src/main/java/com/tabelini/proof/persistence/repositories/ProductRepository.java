package com.tabelini.proof.persistence.repositories;

import com.tabelini.proof.model.Product;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Spring Data Product repository
 */
public interface ProductRepository extends JpaRepository<Product,Long> {

    //TODO:make a JPA repository
    //TODO:make a JDBC repository
}
