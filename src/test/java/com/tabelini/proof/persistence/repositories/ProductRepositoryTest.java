package com.tabelini.proof.persistence.repositories;

import com.tabelini.proof.model.Image;
import com.tabelini.proof.model.Product;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.MatcherAssert.*;
import static org.hamcrest.Matchers.*;

/**
 * Product Repository Tests
 */

@RunWith(SpringRunner.class)
@DataJpaTest
public class ProductRepositoryTest {

    private static final String DEFAULT_NAME = "some name";
    private static final String DEFAULT_DESCRIPTION = "some description";
    private static final long NON_INITIALIZED_ID = 0L;

    //Used a lower level abstraction to see if spring data is really doing the work correctly
    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private ProductRepository repository;

    @Autowired
    private ImageRepository imageRepository;

    @Before
    public void setUp() throws Exception {
        //Always do the testing on an clean table
        repository.deleteAll();
    }

    @Test
    public void saveShouldPersistAnProductWithoutAnAssociatedImage() {
        Product product = createDummyProduct("", 0, null);
        Product savedProduct = repository.save(product);
        assertThat(savedProduct.getId(), not(NON_INITIALIZED_ID));
        Product retrievedProduct = entityManager.find(Product.class, savedProduct.getId());
        assertThat(retrievedProduct.getId(), is(savedProduct.getId()));
        validateFields(retrievedProduct, DEFAULT_NAME, DEFAULT_DESCRIPTION, null);
    }

    @Test
    public void saveShouldPersistAnProductWithAnAssociatedImages() {
        Product product = createDummyProduct("", 3, null);
        Product savedProduct = repository.save(product);
        assertThat(savedProduct.getId(), not(NON_INITIALIZED_ID));
        for (Image image : savedProduct.getImageList()) {
            assertThat(image.getId(), not(NON_INITIALIZED_ID));
        }
        Product retrievedProduct = entityManager.find(Product.class, savedProduct.getId());
        assertThat(retrievedProduct.getId(), is(savedProduct.getId()));
        validateFields(retrievedProduct, DEFAULT_NAME, DEFAULT_DESCRIPTION, savedProduct.getImageList());
    }

    @Test
    public void saveShouldPersistAnParentProductAndSubProducts() {
        Product parentProduct = createDummyProduct("", 3, null);
        Product product1 = createDummyProduct("1", 3, parentProduct);
        Product product2 = createDummyProduct("2", 3, parentProduct);
        Product product3 = createDummyProduct("3", 3, parentProduct);
        List<Product> childProducts = new ArrayList<>();
        childProducts.add(product1);
        childProducts.add(product2);
        childProducts.add(product3);
        parentProduct.setChildProductList(childProducts);
        Product savedParentProduct = repository.save(parentProduct);
        for (Product childProduct : childProducts) {
            assertThat(childProduct.getId(), not(NON_INITIALIZED_ID));
            assertThat(childProduct.getParentProduct().getId(), is(savedParentProduct.getId()));
        }
    }

    @Test
    public void saveShouldUpdateTheImagesOfAProduct() {
        Product product = createDummyProduct("", 3, null);
        repository.save(product);
        Image image = new Image("image4", product);
        product.getImageList().add(image);
        repository.save(product);
        Product retrievedProduct = entityManager.find(Product.class, product.getId());
        assertThat(retrievedProduct.getImageList(), hasSize(4));
        assertThat(retrievedProduct.getImageList().stream().filter(i -> "image4".equals(i.getType())).count(),is(1L));
    }

    @Test
    public void findByIdShouldFindAProduct(){
        Product product = createDummyProduct("",3, null);
        Product savedProduct = entityManager.persist(product);
        Product retrievedProduct = repository.findOne(savedProduct.getId());
        assertThat(retrievedProduct,equalTo(retrievedProduct));
    }


    @Test
    public void deleteShouldDeleteTheCorrectProductAndImages(){
        Product product1 = createDummyProduct("1",3,null);
        Product product2 = createDummyProduct("2",3,null);
        Product savedProduct1 = repository.save(product1);
        Product savedProduct2 = repository.save(product2);
        assertThat(imageRepository.count(),is(6L));
        repository.delete(savedProduct1);
        assertThat(imageRepository.count(),is(3L));
        assertThat(imageRepository.findAll().stream()
                .filter(i -> i.getProduct().getId() == savedProduct2.getId()).count(), is(3L));
    }

    @Test
    public void deleteAParentProductShouldCascade(){
        Product product = createDummyProduct("", 3, null);
        Product product1 = createDummyProduct("1",3,product);
        Product product2 = createDummyProduct("2",3,product);
        List<Product> childProductList = new ArrayList<>();
        childProductList.add(product1);
        childProductList.add(product2);
        product.setChildProductList(childProductList);
        Product savedProduct = repository.save(product);
        assertThat(repository.count(), is(3L));
        assertThat(imageRepository.count(),is(9L));
        repository.delete(savedProduct);
        assertThat(repository.count(), is(0L));
        assertThat(imageRepository.count(),is(0L));
    }

    private Product createDummyProduct(String suffix, int images, Product parentProduct) {
        Product product = new Product(DEFAULT_NAME + suffix, DEFAULT_DESCRIPTION + suffix);
        product.setParentProduct(parentProduct);
        List<Image> imageList = new ArrayList<>();
        for (int i = 0; i < images; i++) {
            imageList.add(new Image("image" + i, product));
        }
        product.setImageList(imageList);
        return product;
    }

    /**
     * validate fields in a separated method to DRY as possible
     */
    private void validateFields(Product product, String name, String description, List<Image> images) {
        assertThat(product.getName(), is(name));
        assertThat(product.getDescription(), is(description));
        for (Image image : product.getImageList()) {
            assertThat(image.getId(), not(NON_INITIALIZED_ID));
            assertThat(image.getProduct().getId(), is(product.getId()));
        }
        if (images != null) {
            assertThat(product.getImageList(), hasSize(images.size()));
            assertThat(product.getImageList(), containsInAnyOrder(images.toArray()));
        }
    }


}