package com.tabelini.proof.endpoint;

import com.tabelini.proof.model.Image;
import com.tabelini.proof.model.Product;
import com.tabelini.proof.service.ProductService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.*;
import static org.hamcrest.Matchers.*;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ProductEndpointTest {

    private static final String DEFAULT_NAME = "some name";
    private static final String DEFAULT_DESCRIPTION = "some description";
    public static final long DEFAULT_ID = 1L;

    @MockBean
    private ProductService productService;

    @Autowired
    private ProductEndpoint endpoint;

    @Before
    public void setUp() throws Exception {

    }

    @Test
    public void getProductByIdShouldReturnTheProductFromService() {
        when(productService.findById(DEFAULT_ID, true)).thenReturn(createProductWithRelationships());
        Response response = endpoint.getProductById(1L);
        verify(productService, times(1)).findById(DEFAULT_ID, true);
        assertThat(response.getStatus(), is(200));
        Product product = (Product) response.getEntity();
        assertThat(product, equalTo(createProductWithRelationships()));
    }

    @Test
    public void getProductByIdThatDoesNotExistShouldReturn404() {
        Response response = endpoint.getProductById(1L);
        verify(productService, times(1)).findById(DEFAULT_ID, true);
        assertThat(response.getStatus(), is(404));
        Map<String, String> res = (Map<String, String>) response.getEntity();
        assertThat(res.get("message"), equalTo("product not found"));
    }

    @Test
    public void createProductShouldCallTheRepoAndReturn201() {
        Product product = new Product(DEFAULT_NAME, DEFAULT_DESCRIPTION);
        when(productService.save(product)).thenReturn(product);
        Response response = endpoint.createProduct(product);
        verify(productService, times(1)).save(product);
        assertThat(response.getStatus(), is(201));
        assertThat(response.getEntity(), equalTo(product));
    }

    @Test
    public void createProductShouldNotAcceptAnAlreadySavedEntity() {
        Product product = new Product(DEFAULT_NAME, DEFAULT_DESCRIPTION);
        product.setId(DEFAULT_ID);
        when(productService.save(product)).thenReturn(product);
        Response response = endpoint.createProduct(product);
        verify(productService, times(0)).save(product);
        assertThat(response.getStatus(), is(400));
        Map<String, String> res = (Map<String, String>) response.getEntity();
        assertThat(res.get("message"), equalTo("product already persisted"));
    }

    @Test
    public void updateProductShouldCallTheRepoAndReturn202() {
        Product product = new Product(DEFAULT_NAME, DEFAULT_DESCRIPTION);
        when(productService.findById(DEFAULT_ID, true)).thenReturn(product);
        product.setId(DEFAULT_ID);
        when(productService.save(product)).thenReturn(product);
        Response response = endpoint.updateProduct(product);
        verify(productService, times(1)).save(product);
        assertThat(response.getStatus(), is(202));
        assertThat(response.getEntity(), equalTo(product));
    }

    @Test
    public void updateProductShouldNotAcceptAnNotSavedEntity() {
        Product product = new Product(DEFAULT_NAME, DEFAULT_DESCRIPTION);
        Response response = endpoint.updateProduct(product);
        verify(productService, times(0)).save(product);
        assertThat(response.getStatus(), is(400));
        Map<String, String> res = (Map<String, String>) response.getEntity();
        assertThat(res.get("message"), equalTo("product not persisted"));
    }

    @Test
    public void updateProductShouldNotAcceptAnEntityThatCannotBeFound() {
        Product product = new Product(DEFAULT_NAME, DEFAULT_DESCRIPTION);
        product.setId(DEFAULT_ID);
        when(productService.save(product)).thenReturn(product);
        Response response = endpoint.updateProduct(product);
        verify(productService, times(0)).save(product);
        assertThat(response.getStatus(), is(404));
        Map<String, String> res = (Map<String, String>) response.getEntity();
        assertThat(res.get("message"), equalTo("product not found"));
    }

    @Test
    public void deleteProductShouldCallTheRepoAndReturn202() {
        Product product = new Product(DEFAULT_NAME, DEFAULT_DESCRIPTION);
        product.setId(DEFAULT_ID);
        when(productService.findById(DEFAULT_ID)).thenReturn(product);
        when(productService.deleteById(product.getId())).thenReturn(product);
        Response response = endpoint.deleteProduct(product.getId());
        verify(productService, times(1)).deleteById(product.getId());
        assertThat(response.getStatus(), is(202));
        assertThat(response.getEntity(), equalTo(product));
    }

    @Test
    public void deleteProductShouldNotAcceptAnEntityThatCannotBeFound() {
        Product product = new Product(DEFAULT_NAME, DEFAULT_DESCRIPTION);
        product.setId(DEFAULT_ID);
        Response response = endpoint.deleteProduct(DEFAULT_ID);
        assertThat(response.getStatus(), is(404));
        Map<String, String> res = (Map<String, String>) response.getEntity();
        assertThat(res.get("message"), equalTo("product not found"));
    }

    public Product createProductWithRelationships() {
        Product product = createDummyProduct(1, "", 3, null);
        Product product1 = createDummyProduct(2, "1", 3, product);
        Product product2 = createDummyProduct(3, "2", 3, product);
        Product product3 = createDummyProduct(4, "3", 3, product);
        List<Product> childProductList = new ArrayList<>();
        childProductList.add(product1);
        childProductList.add(product2);
        childProductList.add(product3);
        product.setChildProductList(childProductList);
        return product;
    }

    public Product createDummyProduct(long id, String suffix, int images, Product parentProduct) {
        Product product = new Product(DEFAULT_NAME + suffix, DEFAULT_DESCRIPTION + suffix);
        product.setId(id);
        product.setParentProduct(parentProduct);
        List<Image> imageList = new ArrayList<>();
        for (int i = 0; i < images; i++) {
            imageList.add(new Image("type" + i, product));
        }
        product.setImageList(imageList);
        return product;
    }


}