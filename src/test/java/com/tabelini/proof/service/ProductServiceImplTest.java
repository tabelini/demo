package com.tabelini.proof.service;

import com.tabelini.proof.model.Product;
import com.tabelini.proof.persistence.repositories.ProductRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.*;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ProductServiceImplTest {

    public static final long DEFAULT_ID = 1L;
    public static final String DEFAULT_DESCRIPTION = "default description";
    public static final String DEFAULT_NAME = "default name";

    public Product defaultProduct = null;

    @MockBean
    private ProductRepository repository;

    @Autowired
    private ProductService service;

    @Before
    public void setUp() throws Exception {
        defaultProduct = new Product(DEFAULT_NAME, DEFAULT_DESCRIPTION);
        defaultProduct.setId(DEFAULT_ID);
        when(repository.findOne(DEFAULT_ID)).thenReturn(defaultProduct);
    }

    @Test
    public void findByIdShouldCallTheRepository(){
        Product product = service.findById(DEFAULT_ID);
        assertThat(product, equalTo(defaultProduct));
        verify(repository,times(1)).findOne(DEFAULT_ID);
    }

    @Test
    public void saveShouldCallTheRepository(){
        service.save(defaultProduct);
        verify(repository,times(1)).save(defaultProduct);
    }

    @Test
    public void deleteByIdShouldCallTheRepository(){
        service.deleteById(DEFAULT_ID);
        verify(repository,times(1)).delete(defaultProduct.getId());
    }

    @Test
    public void findAllShouldCallTheRepository(){
        service.findAll();
        verify(repository,times(1)).findAll();
    }

}