SpringBoot, JAX-RS Demo Project

# Project Definitions

In this project convention over configuration was used as much as possible

Used Lombok to demonstrate its usage, but because of the circular references i had to override the toString,
hash and equals methods

For persistence, spring data was used with hibernate, the database of choice was H2 - the database is automatically 
created and dropped automatically by hibernate, not possible to use it in production but for your tests its ideal

For the JAX-RS implementation was used Jersey

Responses was used to have fine control over the reponses and was faster(at least initially) to send json errors


# Running

To Run the tests use mvn test

To Run the application use mvn spring-boot:run

#Using the REST Api

##CRUD repository

###CREATE
without relationships
curl -X POST -H "Content-Type: application/json" -d '{"id":"","name":"some name post","description":"some description post","imageList":[],"parentProduct":null,"childProductList":[]}' http://localhost:8080/v1/products

with image relationships
curl -X POST -H "Content-Type: application/json" -d '{"id":"","name": "some name post","description": "some description post","imageList":[{"type": "type0","product": ""},{"type": "type1","product": ""}],"childProductList": [],"parentProduct": {"id": 1,"name": "some name","description": "some description","imageList": null,"parentProduct": null,"childProductList": null}}' http://localhost:8080/v1/products

with parent relationships
curl -X POST -H "Content-Type: application/json" -d '{"id":"","name":"some name post","description":"some description post","imageList":[{"type":"type0","product":""},{"type":"type1","product":""}],"childProductList":[],"parentProduct":{"id":1,"name":"some name","description":"some description","imageList":null,"parentProduct":null,"childProductList":null}}' http://localhost:8080/v1/products


###READ
curl http://localhost:8080/v1/products/{ID}


###UPDATE
curl -X PUT -H "Content-Type: application/json" -d '{"id":3,"name":"some name3","description":"some description2","imageList":[{"type":"type10","product":3,"id":4},{"type":"type1","product":3,"id":5},{"type":"type20","product":3,"id":6}],"childProductList":[]}' http://localhost:8080/v1/products


###DELETE
curl -X DELETE -H "Content-Type: application/json" http://localhost:8080/v1/products/{ID}

Extra methods

###GET ALL WITH RELATIONSHIPS(REPEATED ENTITIES RETURNS ONLY THE ID)
curl http://localhost:8080/v1/products

###GET ALL WITHOUT RELATIONSHIPS
curl http://localhost:8080/v1/products/no_relation


###GET WITH RELATIONSHIPS(SINGLE ENTITY)
curl http://localhost:8080/v1/products/{ID}

###GET ALL WITHOUT RELATIONSHIPS
curl http://localhost:8080/v1/products/{ID}/no_relation

###GET PRODUCT IMAGES
http://localhost:8080/v1/products/{ID}/images

###GET PRODUCT IMAGES
http://localhost:8080/v1/products/{ID}/child